﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CBIR_Project
{
    public partial class WorkingDirs : Form
    {
        private List<string> dirList;
        public WorkingDirs()
        {
            InitializeComponent();
            dirList = new List<string>();
        }
        public void SetBox(List<string> vals)
        {
            dirList = vals;
            listBox1.DataSource = dirList;
        }
    }
}
