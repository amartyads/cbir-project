﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using MongoDB.Bson;
using MongoDB.Driver;
using Svg;

namespace CBIR_Project
{
    public partial class Form1 : Form
    {
        static IMongoClient _client;
        static IMongoDatabase _database;
        List<BsonDocument> result;
        List<string> workingDirs = new List<string>();
        public Form1()
        {
            InitializeComponent();
            _client = new MongoClient();
            _database = _client.GetDatabase("CBIRtest");
        }
        private void addToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            /*LocalDirPopup subpop = new LocalDirPopup();
            subpop.ShowDialog();
            string direc = subpop.Direc;
            //MessageBox.Show(direc);
            autoInsertMetadata(direc);*/
            DialogResult res = folderBrowserDialog1.ShowDialog();
            if(res == DialogResult.OK)
            {
                string dir = folderBrowserDialog1.SelectedPath;
                workingDirs.Add(dir);
                autoInsertMetadata(dir);
                populateDB(dir);
            }
        }
        private void autoInsertMetadata(string direc)
        {
            string fool = "A";
            //MessageBox.Show(Path.GetFileName(direc));
            try
            {
                string[] subd = Directory.GetDirectories(direc);
                foreach (string temp in subd)
                {
                    string des1 = Path.GetFileName(temp);
                    string[] pfiles = Directory.GetFiles(temp, "*.svg");
                    foreach (string fle in pfiles)
                    {
                        fool = fle;
                        string des2 = Path.GetFileNameWithoutExtension(fle);
                        insertDesc(fle, des1, des2);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("exception "+fool);
            }
            //MessageBox.Show("Done updating metadata");
        }
        private void insertDesc(string path, string descr)
        {
            insertDesc(path, descr, descr);
        }
        private void insertDesc(string path, string descr, string title)
        {
            XmlDocument pic = new XmlDocument();
            string loc = path;
            pic.Load(loc);
            //XmlNodeList node = pic.ChildNodes;
            XmlNamespaceManager nsMgr = new XmlNamespaceManager(pic.NameTable);
            string strNS = pic.DocumentElement.NamespaceURI;
            nsMgr.AddNamespace("ns", strNS);
            XmlNodeList node = pic.SelectNodes("/ns:svg/ns:desc", nsMgr);
            if (node.Count != 0)
            {
                //MessageBox.Show("desc" + path);
            }
            else
            {
                //MessageBox.Show("no desc");
                XmlNode root = pic.SelectSingleNode("/ns:svg", nsMgr);
                XmlNode desc = pic.CreateNode(XmlNodeType.Element, "desc", strNS);
                desc.InnerText = descr;
                root.PrependChild(desc);
            }
            node = pic.SelectNodes("/ns:svg/ns:title", nsMgr);
            if (node.Count != 0)
            {
                //MessageBox.Show("title" + path);
            }
            else
            {
                //MessageBox.Show("no desc");
                XmlNode root = pic.SelectSingleNode("/ns:svg", nsMgr);
                XmlNode titl = pic.CreateNode(XmlNodeType.Element, "title", strNS);
                titl.InnerText = title;
                root.PrependChild(titl);
            }
            pic.Save(loc);
        }
        private void populateDB(string direc)
        {
            var collect = _database.GetCollection<BsonDocument>("svgfiles");
            //MessageBox.Show(Path.GetFileName(direc));
            XmlDocument pic = new XmlDocument();
            try
            {
                string[] subd = Directory.GetDirectories(direc);
                foreach (string temp in subd)
                {
                    string[] pfiles = Directory.GetFiles(temp, "*.svg");
                    foreach (string fle in pfiles)
                    {
                        pic.Load(fle);
                        XmlNamespaceManager nsMgr = new XmlNamespaceManager(pic.NameTable);
                        string strNS = pic.DocumentElement.NamespaceURI;
                        nsMgr.AddNamespace("ns", strNS);
                        string title = pic.SelectSingleNode("/ns:svg/ns:title", nsMgr).InnerText;
                        string desc = pic.SelectSingleNode("/ns:svg/ns:desc", nsMgr).InnerText;
                        string textInFile = getTextFromSVG(pic, nsMgr);

                        var document = new BsonDocument
                        {
                            {"filename", Path.GetFileNameWithoutExtension(fle)},
                            {"filepath", fle},
                            {"filetype", "svg" },
                            {"title", title.ToLower()},
                            {"desc", desc.ToLower()},
                            {"text", textInFile.ToLower() }
                        };
                        collect.InsertOne(document);
                    }
                }

            }
            catch (Exception)
            {
                MessageBox.Show("exception");
            }
            MessageBox.Show("Done updating database");
        }
        private string getTextFromSVG(XmlDocument pic, XmlNamespaceManager nsMgr)
        {
            string allText = "";
            string gs = "";
            bool exit = false;
            while (!exit)
            {
                XmlNodeList toxts = pic.SelectNodes("/ns:svg" + gs + "/ns:text", nsMgr);
                foreach (XmlNode textnode in toxts)
                {
                    allText += textnode.InnerText + " ";
                }
                XmlNodeList nodes = pic.SelectNodes("/ns:svg" + gs + "/ns:g", nsMgr);
                if (nodes.Count == 0)
                    exit = true;
                else
                    gs += "/ns:g";
            }
            StringBuilder sb = new StringBuilder();
            string[] parts = allText.Split(new char[] { ' ', '\n', '\t', '\r', '\f', '\v', '\\' }, StringSplitOptions.RemoveEmptyEntries);
            int size = parts.Length;
            for (int i = 0; i < size; i++)
                if (parts[i].Length > 2)
                    sb.AppendFormat("{0} ", parts[i]);
            allText = sb.ToString().Trim();
            return allText;
        }
        private void viewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkingDirs pop = new WorkingDirs();
            pop.SetBox(workingDirs);
            pop.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            searchDB(metadatabox.Text.Split(' '), metadatabox.Text.Split(' '), metadatabox.Text.Split(' '));
        }

        private void searchDB(string[] text1, string[] text2, string[] text3)
        {
            var collect = _database.GetCollection<BsonDocument>("svgfiles");
            var builder = Builders<BsonDocument>.Filter;
            var filter = builder.Eq("title","init");
            result = null;
            foreach (string temp in text1)
            {
                filter = builder.Regex("title", new BsonRegularExpression("/" + temp + "/"));
                if (result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
                else result = collect.Find(filter).ToList();
            }
            List<string> files = new List<string>();
            int noOfRes = Convert.ToInt32(numericUpDown1.Value);
            if (result == null || result.Count < noOfRes)
            {
                foreach (string temp in text2)
                {
                    filter = builder.Regex("desc", new BsonRegularExpression("/" + text2 + "/"));
                    try
                    {
                        List<BsonDocument> result2 = collect.Find(filter).ToList();
                        if (result != null) result = result.Union(result2).ToList();
                        else result = result2.ToList();
                    }
                    catch (Exception) { }
                }
                if (result != null && result.Count > noOfRes) { }
                else
                {
                    foreach (string temp in text3)
                    {
                        if (temp != "")
                        {
                            filter = builder.Regex("text", new BsonRegularExpression("/" + temp + "/"));
                            try
                            {
                                if(result != null) result = result.Union(collect.Find(filter).ToList()).ToList();
                                else result = collect.Find(filter).ToList();
                            }
                            catch (Exception) { }
                        }
                    }
                }
            }
            for (int i = 0; i < Math.Min(noOfRes, result.Count); i++)
            {
                string temp = result.ElementAt(i).GetElement("filename").ToString();
                temp = temp.Substring(temp.IndexOf('=') + 1);
                files.Add(temp);
            }
            if (files.Count == 0) files.Add("No search results");
            listBox1.DataSource = files;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sel = listBox1.SelectedItem.ToString();
            if (sel.Equals("No search results")) { }
            else
            {
                var collect = _database.GetCollection<BsonDocument>("svgfiles");
                var builder = Builders<BsonDocument>.Filter;
                var filter = builder.Eq("filename", sel);
                var curFile = collect.Find(filter).ToList();
                string curPath = curFile.ElementAt(0).GetElement("filepath").ToString();
                curPath = curPath.Substring(curPath.IndexOf('=') + 1);
                webBrowser1.Navigate(curPath);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "svg files (*.svg)|*.svg";
            DialogResult res = openFileDialog1.ShowDialog();
            if(res == DialogResult.OK)
            {
                string file = openFileDialog1.FileName;
                string[] title, desc, text;

                XmlDocument pic = new XmlDocument();
                string loc = file;
                pic.Load(loc);
                //XmlNodeList node = pic.ChildNodes;
                XmlNamespaceManager nsMgr = new XmlNamespaceManager(pic.NameTable);
                string strNS = pic.DocumentElement.NamespaceURI;
                nsMgr.AddNamespace("ns", strNS);
                XmlNode node = pic.SelectSingleNode("/ns:svg/ns:desc", nsMgr);
                desc = node.InnerText.Split(' ');
                node = pic.SelectSingleNode("/ns:svg/ns:title", nsMgr);
                title = node.InnerText.Split(' ');
                text = getTextFromSVG(pic, nsMgr).Split(' ');
                for (int i = 0; i < title.Length; i++)
                {
                    title[i] = title[i].ToLower();
                }
                for (int i = 0; i < desc.Length; i++)
                {
                    desc[i] = desc[i].ToLower();
                }
                for (int i = 0; i < text.Length; i++)
                {
                    text[i] = text[i].ToLower();
                }
                //query
                searchDB(title, desc, text);
            }
        }
    }
}